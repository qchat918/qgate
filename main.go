package main

import (
	"log"
	"fmt"
	"net/http"
	"html"
)

func Home(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "Home, %q", html.EscapeString(r.URL.Path))
}

func Api(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "path, %q", html.EscapeString(r.URL.Path))
}

func Login(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "path, %q", html.EscapeString(r.URL.Path))
}

func Regist(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "path, %q", html.EscapeString(r.URL.Path))
}


func Logout(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "path, %q", html.EscapeString(r.URL.Path))
}

func main(){
	
	http.HandleFunc("/",Home)
	
	http.HandleFunc("/api",Api)

	http.HandleFunc("/api/v1/regist",Regist)

	http.HandleFunc("/api/v1/login",Login)

	http.HandleFunc("/api/v1/logout",Logout)

	log.Fatal(http.ListenAndServe(":9999", nil))

}